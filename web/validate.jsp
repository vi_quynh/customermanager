<%-- 
    Document   : validate.jsp
    Created on : May 27, 2018, 7:36:59 AM
    Author     : viquy
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Validate Page</title>
    </head>
    <body>
        <h1>The export data is invalid</h1>
        <h4>${errors}</h4>
    </body>
</html>
