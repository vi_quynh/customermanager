<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : customers.xsl
    Created on : May 25, 2018, 4:35 PM
    Author     : viquy
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <body>
                <h2>Display data</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>ContactName</th>
                        <th>ContactTitle</th>
                        <th>CompanyName</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>PostalCode</th>
                        <th>Country</th>
                        <th>Phone</th>
                        <th>Fax</th>
                    </tr>
                    <xsl:for-each select="Customers/Customer">
                        <tr>
                            <td>
                                <xsl:value-of select="ContactName"/>
                            </td>
                            <td>
                                <xsl:value-of select="ContactTitle"/> 
                            </td>
                            <td>
                                <xsl:value-of select="CompanyName"/>
                            </td>
                            <td>
                                <xsl:value-of select="Address"/>
                            </td>
                            <td>
                                <xsl:value-of select="City"/>
                            </td>
                            <td>
                                <xsl:value-of select="PostalCode"/>
                            </td>
                            <td>
                                <xsl:value-of select="Country"/>
                            </td>
                            <td>
                                <xsl:value-of select="Phone"/>
                            </td>
                            <td>
                                <xsl:value-of select="Fax"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
