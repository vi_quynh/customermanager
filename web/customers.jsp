<%-- 
    Document   : customers
    Created on : May 25, 2018, 3:56:44 PM
    Author     : viquy
--%>

<%@page contentType="text/xml" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<?xml-stylesheet type="text/xsl" href="customers.xsl"?>
<Customers>
    <c:forEach var="customer" items="${customers}">
        <Customer>
        <ContactName>${customer.contactName}</ContactName>
        <ContactTitle>${customer.contactTitle}</ContactTitle>
        <CompanyName>${customer.companyName}</CompanyName>
        <Address>${customer.address}</Address>
        <City>${customer.city}</City>
        <PostalCode>${customer.postalCode}</PostalCode>
        <Country>${customer.country}</Country>
        <Phone>${customer.phone}</Phone>
        <Fax>${customer.fax}</Fax>
        </Customer>
    </c:forEach>
</Customers>
